
General principles
==================

General principles
------------------

An application subscribes to messages from other applications by sending them a Perl regular expression.
Each regex is attached to a callback. The callback is called when a received message match the regex, with captured groups as arguments.

The matching is done on the sender side : when an application sends a message, the message is matched against all regular expressions it gots, and only the captured groups are sent to other applications.


Example 1
---------

Lets say we have 2 applications connected on the bus :

+ **welcome** that subscribed to ``^Hello, I'm (.*).``
+ **client1** that subscribed to ``(.*)``

If *client1* wants to send the message ``Hello, I'm John.``, it will match it against the regex from *welcome* and extract and send the captured groups. In this case, only ``John`` will be sent to *welcome*. *welcome* will execute its corresponding callback with ``John`` as argument.

If in return *welcome* answers with ``Welcome John!``, *answer* will match this message against *client1* regex. Since *client1* regex match any message (this is a bad practice), the complete message will be send.
    

Example 2
---------

Lets say we have 3 applications connected on the bus :

+ **temp logger** that subscribed to ``^.*temp:(.*)°C.*``
+ **moisture logger** that subscribed to ``^.*moist:(.*)%.*``
+ **sensor** that subscribed to ``^sensor start ([0-9]+)`` and ``^sensor stop``

If *sensor* sends ``temp:18°C``, *temp logger* will receive a message that will trigger it first (and only) callback with ``18`` as argument.

If *sensor* sends ``temp:15°C, moist:68%``, *temp logger* triggers its 1st callback with ``15`` as argument, and *moist logger* trigger its 1st callback with ``68`` as argument.

If *temp logger* or *moisture logger* send ``sensor start 5``, *sensor* tiggers its 1st callback with ``5`` as argument.

If *temp logger* or *moisture logger* send ``sensor stop``, *sensor* tiggers its 2nd callback without any arguments.


