
Leaving an Ivy bus
==================

An application about to leave an Ivy bus should send the :ref:`bye` message before closing the connections is has with its peers. However this is not mandatory.
It then close its UDP and TCP sockets.

This implies that applications should gracefully handle the closing of a connection by a peer.

