
Ivy protocol messages
=====================

Messages format
---------------

Once the TCP stream is established between two Ivy applications, they begin to exchange messages.There are 11 types of messages, all following the same pattern. All messages are text messages in ASCII. 

They are composed of:

+ the message type (from 0 to 10)
+ a white space
+ a numerical identifier, used only in some message types
+ the STX character (ASCII code 2)
+ parameters, that differ from message type to message type
+ the LF character (ASCII code 10, ’\n’ in the C language).

When the identifier is not meaningful, it should be set to **0**.

Messages
--------

.. _bye:

Bye
___

+ **type**: 0
+ **identifier**: not meaningful
+ **parameter**: none

This notifies the receiving application that the sending application is disconnecting, and advises it to close its side of the stream socket.

.. _subscription:

Subscription
____________

+ **type**: 1
+ **identifier**: an unique numerical identifier for the subscription, chosen by the application that sends it.
+ **parameter**: the regular expression that defines the subscription.

The identifier will be used further when the sending application unsubscribes to that regular expression.

.. note::
    If the identifier is already used, the previous regex is overwriten.

.. _text_message:

Text message
____________

+ **type**: 2
+ **identifier**: identifier of the subscription that was sent by the peer, and whose regular expression matches the text message the application wants to send.
+ **parameter**: substrings of the text message that were captured by the regular expression, each one followed by ETX (ASCIIcode 3) characters. If there is no capture in the regular expression, no parameter is sent.

.. note::
    Each substring must be followed by ETX, so there is as many ETX characters than captured substrings, even if some captures are empty.
    
    An example with a capture would be ``hello (.*)``. Then, for a ``hello world`` message, there would be one parameter in the message: ``world<ETX>``.

.. _error:

Error
_____

+ **type**: 3
+ **identifier**: not meaningful
+ **parameter**: a text that describes the protocol error encountered by the application which sends it.

.. _subscription_deletion:

Subscription deletion
_____________________

+ **type**: 4
+ **identifier**: the identifier of the subscription to be deleted.
+ **parameter**: none

.. _end_of_initial_subscriptions:

End of initial subscriptions
____________________________

+ **type**: 5
+ **identifier**: not meaningful
+ **parameter**: none

.. _peerID:

Peer ID
_______

+ **type**: 6
+ **identifier**: port number of the TCP socket of the application that emits the message
+ **parameter**: name of the application that sends the message

The port number (identifier) combined with the address of the computer that emits this message (which can be deduced by POSIX system calls) provides an unique ID for the application.
This ID will be used to avoid double connexions, as each application is both client and server.


.. _direct_message:

Direct Message
______________

+ **type**: 7
+ **identifier**: any
+ **parameter**: any

The identifier will be passed to the peer, as well as the parameter.

.. _quit:

Quit
____

+ **type**: 8
+ **identifier**: not meaningful
+ **parameter**: none

This asks the peer application to quit.

.. _ping:

Ping
____

+ **type**: 9
+ **identifier**: ping id
+ **parameter**: none

.. _pong:

Pong
____

+ **type**: 10
+ **identifier**: ping id received from the corresponding Ping message this pong answer to.
+ **parameter**: none



