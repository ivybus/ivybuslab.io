
Establishing connections
========================

Ivy involves no server. Communication among applications is point-to-point, through TCP sockets: an Ivy bus is actually a set of applications that are connected to each other in a complete graph.

Application joining a bus
-------------------------

+ The application broadcast a UDP message to signal its existence and the address of the TCP port were it can be contacted (this implies that there can exist one Ivy bus per available UDP port on the network). It also provide a *watcherId* to be use by the application to not connect to itself, as it will receive its own UDP message.

  The UDP message is formatted as follow, the parameters being space separated: 
  ``<protocol version> <TCP port> <watcherId> <application name>``

  The current protocol version is **3**.

  example: ``3 32597 rand0m_45d6sf45sdf myApp``

  .. note:: Like all Ivy messages, this message **must** end with a newline caracter (``\n``).

+ All present applications on the bus reply by establishing a socket connection.
+ Once connected, each peer application:
    + sends the :ref:`peerID` message that will provide an unique ID for identifing this application.
    + sends its initial subscriptions with :ref:`subscription` messages
    + sends the :ref:`end_of_initial_subscriptions` message
+ For each peer application that established connection, the joining application must:
    + sends its initial subscriptions with :ref:`subscription` messages
    + sends the :ref:`end_of_initial_subscriptions` message

The application is then fully connected.

Application already connected to the bus
----------------------------------------

When a peer application joins the bus, it sends a UDP message, and all applications already connected to the bus must execute the reciprocal actions.

An application that was already on the bus must then:

+ connects to the peer's TCP socket
+ sends the :ref:`peerID` message.
+ sends its initial subscriptions with :ref:`subscription` messages
+ sends the :ref:`end_of_initial_subscriptions` message
+ it also wait from the peer application to:
    + sends peer's initial subscriptions with :ref:`subscription` messages
    + sends the :ref:`end_of_initial_subscriptions` message


