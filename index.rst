
Ivy bus documentation
=====================

Ivy is a software bus that allows applications to broadcast information through text messages, with a subscription mechanism based on regular expressions.

It is fully distributed (it has no central server): each application is connected to all other applications in a complete graph.



.. toctree::
   :maxdepth: 1
   
   general_principles.rst
   protocol_messages.rst
   handshake.rst
   leaving.rst
   



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
