
# Ivy bus documentation

This is the sources of the new documentation for the Ivy bus.

Check the doc at [https://ivybus.gitlab.io/](https://ivybus.gitlab.io/).

This doc is built using [sphinx](https://www.sphinx-doc.org/) with the [rtd theme](https://github.com/readthedocs/sphinx_rtd_theme).

This doc is an evolution of the old doc at [https://www.eei.cena.fr/products/ivy/documentation/ivy-protocol.pdf](https://www.eei.cena.fr/products/ivy/documentation/ivy-protocol.pdf).

## How to contribute to the doc

+ Install needed python modules: `python3 -m pip install -r requirements.txt`
+ Edit the doc
+ Build it localy with `make html`
+ Check how it renders (home page is in `_build/html/index.html`)
+ Commit and Make a merge request

